'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserPreferenceSchema extends Schema {
  up () {
    this.create('user_preference', table => {
      table.increments()
      table
        .integer('user_id')
        .notNullable()
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table
        .integer('preference_id')
        .unsigned()
        .references('id')
        .inTable('preferences')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.timestamps()
      table.unique(['user_id', 'preference_id'])
    })
  }

  down () {
    this.drop('user_preference')
  }
}

module.exports = UserPreferenceSchema
