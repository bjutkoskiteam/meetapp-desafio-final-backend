'use strict'

const Schema = use('Schema')
const Preference = use('App/Models/Preference')

class PreferenceSchema extends Schema {
  up () {
    this.create('preferences', table => {
      table.increments()
      table.string('description')
      table.timestamps()
    })

    this.schedule(async trx => {
      await Preference.createMany(
        [
          { description: 'Front-end' },
          { description: 'Back-end' },
          { description: 'Mobile' },
          { description: 'DevOps' },
          { description: 'Gestão' },
          { description: 'Marketing' }
        ],
        trx
      )
    })
  }

  down () {
    this.drop('preferences')
  }
}

module.exports = PreferenceSchema
