'use strict'

const Mail = use('Mail')
const Env = use('Env')

class MeetupSubscribeMail {
  // If this getter isn't provided, it will default to 1.
  // Increase this number to increase processing concurrency.
  static get concurrency () {
    return 1
  }

  // This is required. This is a unique key used to identify this job.
  static get key () {
    return 'MeetupSubscribeMail-job'
  }

  // This is where the work is done.
  async handle ({ email, name, title }) {
    console.log(`Job: ${MeetupSubscribeMail.key}`)

    await Mail.send(
      ['emails.meetup_subscription'],
      {
        name,
        title
      },
      message => {
        message
          .to(email)
          .from(Env.get('SEND_MAIL_MAIL'), Env.get('SEND_MAIL_NAME'))
          .subject('Meetapp - Inscrição Realizada')
      }
    )
  }
}

module.exports = MeetupSubscribeMail
