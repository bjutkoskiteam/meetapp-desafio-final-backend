'use strict'

const Kue = use('Kue')
const Job = use('App/Jobs/MeetupSubscribeMail')

const MeetupSubscriptionHook = (exports = module.exports = {})

MeetupSubscriptionHook.sendSubscriptionMail = async meetupSubscriptionInstance => {
  const { email, name } = await meetupSubscriptionInstance.user().fetch()

  const { title } = await meetupSubscriptionInstance.meetup().fetch()

  Kue.dispatch(Job.key, { email, name, title }, { attempts: 3 })
}
