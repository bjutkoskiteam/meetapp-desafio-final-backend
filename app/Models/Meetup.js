'use strict'

const Model = use('Model')

class Meetup extends Model {
  user () {
    return this.belongsTo('App/Models/User')
  }

  subscriptions () {
    return this.hasMany('App/Models/MeetupSubscription')
  }

  preferences () {
    return this.belongsToMany('App/Models/Preference')
      .pivotTable('meetup_preference')
      .withTimestamps()
  }

  file () {
    return this.belongsTo('App/Models/File')
  }
}

module.exports = Meetup
