'use strict'

const Database = use('Database')
const Meetup = use('App/Models/Meetup')
const MeetupSubscription = use('App/Models/MeetupSubscription')
const User = use('App/Models/User')

class MeetupController {
  async index ({ request }) {
    let { title } = request.get()
    if (!title) title = ''
    const meetups = await Meetup.query()
      .where('title', 'ilike', '%' + title + '%')
      .orderBy('date', 'desc')
      .with('file')
      .with('subscriptions')
      .fetch()

    return meetups
  }

  async coming () {
    const meetups = await Meetup.query()
      .with('file')
      .with('subscriptions')
      .orderBy('date', 'desc')
      .fetch()

    return meetups
  }

  async subscribed ({ auth }) {
    const subscriptions = await MeetupSubscription.query()
      .where({
        user_id: auth.user.id
      })
      .fetch()

    const meetups = await Meetup.query()
      .whereIn('id', subscriptions.toJSON().map(s => s.meetup_id))
      .with('file')
      .with('subscriptions')
      .orderBy('date', 'desc')
      .fetch()

    return meetups
  }

  async recommended ({ auth }) {
    const user = await User.findOrFail(auth.user.id)
    const preferences = await user.preferences().fetch()

    const meetupsPreferences = await Database.from('meetup_preference')
      .whereIn('preference_id', preferences.toJSON().map(p => p.id))
      .distinct('meetup_id')

    const meetups = await Meetup.query()
      .whereIn('id', meetupsPreferences.map(m => m.meetup_id))
      .with('file')
      .with('subscriptions')
      .orderBy('date', 'desc')
      .fetch()

    return meetups
  }

  async store ({ request, auth }) {
    const data = request.only([
      'title',
      'description',
      'place',
      'date',
      'file_id'
    ])

    const preferences = request.input('preferences')
    const trx = await Database.beginTransaction()

    const meetup = await Meetup.create({ ...data, user_id: auth.user.id }, trx)
    if (preferences) {
      await meetup.preferences().sync(preferences, null, trx)
    }

    await trx.commit()

    return meetup
  }

  async show ({ params, auth }) {
    const meetup = await Meetup.findOrFail(params.id)
    await meetup.load('file')
    await meetup.load('subscriptions')

    const isSubscribed =
      (await MeetupSubscription.query()
        .where({
          meetup_id: params.id,
          user_id: auth.user.id
        })
        .getCount()) > 0
    return { meetup, isSubscribed }
  }

  async update ({ params, request }) {
    const meetup = await Meetup.findOrFail(params.id)
    const data = request.only([
      'title',
      'description',
      'place',
      'date',
      'file_id'
    ])

    const preferences = request.input('preferences')

    const trx = await Database.beginTransaction()

    meetup.merge({ ...data }, trx)
    await meetup.save(trx)
    await meetup.preferences().sync(preferences, null, trx)

    await trx.commit()

    await meetup.load('preferences')

    return meetup
  }

  async destroy ({ params }) {
    const meetup = await Meetup.findOrFail(params.id)

    await meetup.delete()
  }

  async subscribe ({ params, auth }) {
    const data = { meetup_id: params.id, user_id: auth.user.id }
    const meetupSubscription = await MeetupSubscription.create(data)

    return meetupSubscription
  }

  async unsubscribe ({ params, auth }) {
    const affectedRows = await MeetupSubscription.query()
      .where({
        meetup_id: params.id,
        user_id: auth.user.id
      })
      .delete()

    if (affectedRows === 0) {
      throw new Error('Record not found')
    }
  }
}

module.exports = MeetupController
