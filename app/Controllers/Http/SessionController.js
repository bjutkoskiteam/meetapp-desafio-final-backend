'use strict'

const User = use('App/Models/User')

class SessionController {
  async store ({ request, response, auth }) {
    const { email, password } = request.all()

    const token = auth.attempt(email, password)

    return token
  }

  async protected ({ auth }) {
    const user = await User.findOrFail(auth.user.id)
    const preferences = await user.preferences().fetch()

    return {
      email: user.email,
      name: user.name,
      preferences: preferences.toJSON().map(p => p.id)
    }
  }
}

module.exports = SessionController
