'use strict'

const Database = use('Database')
const User = use('App/Models/User')

class UserController {
  async store ({ request }) {
    const data = request.only(['email', 'password', 'name'])
    const preferences = request.input('preferences')

    const trx = await Database.beginTransaction()

    const user = await User.create(data, trx)
    if (preferences) {
      await user.preferences().sync(preferences, null, trx)
    }

    await trx.commit()

    return user
  }

  async update ({ request, auth }) {
    const user = await User.findOrFail(auth.user.id)
    const data = request.only(['email', 'password', 'name'])
    const preferences = request.input('preferences')

    const trx = await Database.beginTransaction()

    user.merge({ ...data })
    await user.save(trx)
    if (preferences) {
      await user.preferences().sync(preferences, null, trx)
    }

    await trx.commit()
    return {
      email: user.email,
      name: user.name,
      preferences
    }
  }
}

module.exports = UserController
