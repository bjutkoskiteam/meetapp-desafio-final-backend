'use strict'

const Route = use('Route')

Route.post('users', 'UserController.store')
Route.post('sessions', 'SessionController.store')

Route.post('passwords', 'ForgotPasswordController.store')
Route.put('passwords', 'ForgotPasswordController.update')
Route.get('files/:id', 'FileController.show')

Route.group(() => {
  Route.get('protected', 'SessionController.protected')

  Route.post('files', 'FileController.store')

  Route.put('users', 'UserController.update')

  Route.get('meetups/coming', 'MeetupController.coming')
  Route.get('meetups/subscribed', 'MeetupController.subscribed')
  Route.get('meetups/recommended', 'MeetupController.recommended')
  Route.get('meetups/:id', 'MeetupController.show')
  Route.get('meetups', 'MeetupController.index')

  Route.post('meetups', 'MeetupController.store')
  Route.post('meetups/:id/subscribe', 'MeetupController.subscribe')

  Route.put('meetups', 'MeetupController.update')

  Route.delete('meetups', 'MeetupController.destroy')
  Route.delete('meetups/:id/subscribe', 'MeetupController.unsubscribe')
}).middleware(['auth'])
